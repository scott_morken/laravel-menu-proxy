<?php
return [
    'menu_service_lookup' => \Lavary\Menu\Menu::class,
    'storage_class'       => Smorken\MenuProxy\Storage\Example::class,
    'proxy_class'         => Smorken\MenuProxy\Proxies\Lavary::class,
    'proxy_options'       => [
        'menu_name'        => 'MainMenu',
        'menu_view'        => 'smorken/menuproxy::menu.lavary.bs4.menu',
        'item_view'        => 'smorken/menuproxy::menu.lavary.bs4.item',
        'subnav_view'      => 'smorken/menuproxy::menu.lavary.bs4.subnav',
        'subnav_menu_view' => 'smorken/menuproxy::menu.lavary.bs4.pills',
        'logo_path'        => null,//'images/logo.png'
    ],
];
