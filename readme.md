## Menu Proxying extension for Laravel 5+

### License

This software is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)

### Installation

Add as a service provider to your config/app.php

    ...
    'providers' => [
            Smorken\MenuProxy\ServiceProvider::class,
    ...
    
Publish the files (if needed)

```
php artisan vendor:publish --provider="Smorken\MenuProxy\ServiceProvider"
```

This will provide a `resource/views/menu` directory and `config/menuproxy.php`
