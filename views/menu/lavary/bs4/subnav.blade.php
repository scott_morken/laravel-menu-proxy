@if (isset($sub) && $sub)
    <?php $mp = app('Smorken\MenuProxy\Contracts\Proxy'); ?>
    <?php $main = $mp->getUsable(); ?>
    <?php $submenu = $main->get($sub); ?>
    @if ($submenu && $submenu->hasChildren())
        @include($mp->getOption('subnav_menu_view'), ['items' => $submenu->children(), 'item_view' => $mp->getOption('item_view')])
    @endif
@endif