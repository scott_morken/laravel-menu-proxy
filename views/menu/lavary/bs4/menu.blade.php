<nav class="navbar navbar-expand-lg navbar-dark" style="background-color: #002B5E;">
    <a class="navbar-brand" href="{{ url('/') }}">
        @if (isset($logo) && $logo)
            <img src="{!! asset($logo) !!}" alt="Logo"/>
        @endif
        {{ config('app.title', 'My App') }}
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav">
            @include($item_view, array('items' => $menu->roots()))
        </ul>
    </div>
</nav>
