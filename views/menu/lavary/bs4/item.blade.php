@foreach($items as $item)
    <li @lm_attrs($item) class="nav-item" @lm_endattrs>
        <a @lm_attrs($item) title="{{ $item->title }} menu option" class="nav-link"
           @lm_endattrs href="{{ $item->url() }}">
            {!! $item->title !!}
        </a>
    </li>
@endforeach
