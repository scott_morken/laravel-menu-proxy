@foreach($items as $item)
    <li @lm_attrs($item) @lm_endattrs>
        <a @lm_attrs($item->link) title="{{ $item->title }} menu option" @lm_endattrs href="{{ $item->url() }}">
            {!! $item->title !!}
        </a>
    </li>
@endforeach
