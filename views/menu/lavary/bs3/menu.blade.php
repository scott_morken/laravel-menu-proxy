<nav class="navbar navbar-default" role="navigation">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#main-nav-bar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{ URL::to('/') }}">
                {{ config('app.title', 'My App') }}
            </a>
        </div>
        <div class="collapse navbar-collapse" id="main-nav-bar">
            <ul class="nav navbar-nav">
                @include($item_view, array('items' => $menu->roots()))
            </ul>
        </div>
    </div>
</nav>
