<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 1/27/16
 * Time: 11:00 AM
 */

namespace Smorken\MenuProxy\Proxies;

use Lavary\Menu\Menu;
use Smorken\MenuProxy\Contracts\Proxy;
use Smorken\MenuProxy\Contracts\Storage;

class Lavary extends Base implements Proxy
{

    protected $options = [
        'menu_name' => 'MainMenu',
    ];

    public function isBuilt()
    {
        return $this->getMenu()->get($this->getOption('menu_name')) !== null;
    }

    public function build()
    {
        if (!$this->isBuilt()) {
            $this->getMenu()->make(
                $this->getOption('menu_name'),
                function ($m) {
                    $this->iterateMenu($this->getProvider()->asArray(), $m);
                }
            );
        }
        return $this->isBuilt();
    }

    protected function iterateMenu($menus, $menu)
    {
        foreach ($menus as $name => $menudata) {
            $extended = $this->createExtendedOptions($menudata);
            $selected = $this->addToMenu($name, $menudata, $menu);
            $this->handleExtendedOptions($extended, $selected);
        }
    }

    protected function handleExtendedOptions($options, $menu)
    {
        foreach ($options as $k => $v) {
            $methodname = 'handle' . ucfirst($k);
            if (method_exists($this, $methodname)) {
                $this->$methodname($k, $v, $menu);
            }
        }
    }

    protected function handleActive($key, $value, $menu)
    {
        if ($value) {
            $menu->active($value);
        }
    }

    protected function handleChildren($key, $value, $menu)
    {
        if ($value && is_array($value)) {
            $this->iterateMenu($value, $menu);
        }
    }

    protected function createExtendedOptions(&$options)
    {
        $extended = [];
        $check = ['children', 'active'];
        foreach ($check as $k) {
            $this->addAndUnset($k, $extended, $options);
        }
        return $extended;
    }

    protected function addAndUnset($key, &$extended, &$options)
    {
        if (array_key_exists($key, $options)) {
            $extended[$key] = $options[$key];
            unset($options[$key]);
        }
    }

    /**
     * @param string $name
     * @param array $data
     * @param $menu
     */
    protected function addToMenu($name, $data, $menu)
    {
        return $menu->add($name, $data);
    }

    public function getUsable()
    {
        $this->build();
        return $this->getMenu()->get($this->getOption('menu_name'));
    }

    public function getView()
    {
        return view($this->getOption('menu_view'))
            ->with('item_view', $this->getOption('item_view'))
            ->with('menu', $this->getUsable())
            ->with('logo', $this->getOption('logo_path'));
    }

    public function render()
    {
        return $this->getView()->render();
    }

    /**
     * $key can be a single string or in dot notation
     * @param $key
     * @return mixed
     */
    public function getSubmenu($key)
    {
        $keys = explode('.', $key);
        $menu = $this->getUsable();
        $s = $menu->all();
        foreach ($keys as $k) {
            $s = $this->getSubmenuCollection($k, $s);
        }
        return $s;
    }

    protected function getSubmenuCollection($key, $coll)
    {
        if (!$coll) {
            throw new \InvalidArgumentException('No collection passed to submenu.');
        }
        $item = $this->getItem($key, $coll);
        if ($item->hasChildren()) {
            return $item->children();
        }
    }

    protected function getItem($key, $coll)
    {
        return $coll->first(
            function ($v, $k) use ($key) {
                return $v->nickname === $key;
            }
        );
    }
}
