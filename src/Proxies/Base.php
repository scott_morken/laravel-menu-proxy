<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 1/27/16
 * Time: 10:58 AM
 */

namespace Smorken\MenuProxy\Proxies;

use Smorken\MenuProxy\Contracts\Storage;

abstract class Base
{

    /**
     * @var Storage
     */
    protected $provider;

    protected $menu;

    protected $options = [
    ];

    /**
     * @param mixed $menu
     * @param array $options
     * @param Storage $provider
     */
    public function __construct($menu, array $options, Storage $provider)
    {
        $this->setMenu($menu);
        $this->setOptions($options);
        $this->setProvider($provider);
    }

    public function setOptions(array $options)
    {
        $this->options = array_merge($this->options, $options);
    }

    public function getMenu()
    {
        return $this->menu;
    }

    public function setMenu($menu)
    {
        $this->menu = $menu;
    }

    /**
     * @return Storage
     */
    public function getProvider()
    {
        return $this->provider;
    }

    /**
     * @param Storage $provider
     */
    public function setProvider(Storage $provider)
    {
        $this->provider = $provider;
    }

    public function getOption($key, $default = null)
    {
        if (array_key_exists($key, $this->options)) {
            return $this->options[$key];
        }
        return $default;
    }
}
