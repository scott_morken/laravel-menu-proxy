<?php
namespace Smorken\MenuProxy\Contracts;

interface Storage
{

    /**
     * @return array
     */
    public function asArray();
}
