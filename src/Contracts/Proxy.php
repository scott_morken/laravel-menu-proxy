<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 1/27/16
 * Time: 10:53 AM
 */

namespace Smorken\MenuProxy\Contracts;

interface Proxy
{

    /**
     * @return bool
     */
    public function isBuilt();

    /**
     * @return bool
     */
    public function build();

    /**
     * @return mixed
     */
    public function getView();

    /**
     * @return string
     */
    public function render();

    /**
     * Return the fully instantiated menu object
     * @return mixed
     */
    public function getUsable();

    /**
     * @param $menu
     * @return null
     */
    public function setMenu($menu);

    /**
     * @return mixed
     */
    public function getMenu();

    /**
     * @param Storage $provider
     * @return null
     */
    public function setProvider(Storage $provider);

    /**
     * @return Storage
     */
    public function getProvider();

    /**
     * @param array $options
     * @return null
     */
    public function setOptions(array $options);

    /**
     * @param $key
     * @param null $default
     * @return mixed
     */
    public function getOption($key, $default = null);

    /**
     * $key can be a single string or in dot notation
     * @param $key
     * @return mixed
     */
    public function getSubmenu($key);
}
