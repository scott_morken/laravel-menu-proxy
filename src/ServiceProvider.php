<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 1/27/16
 * Time: 11:11 AM
 */

namespace Smorken\MenuProxy;

use Illuminate\Support\ServiceProvider as SP;
use Smorken\MenuProxy\Contracts\Proxy;
use Smorken\MenuProxy\Contracts\Storage;

class ServiceProvider extends SP
{

    /**
     * Bootstrap the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__ . '/../views', 'smorken/menuproxy');
        $this->publishes(
            [
                __DIR__ . '/../views' => base_path('/resources/views'),
            ],
            'views'
        );
        $this->bootConfig();
        $this->app->register(\Lavary\Menu\ServiceProvider::class);
    }

    protected function bootConfig()
    {
        $package_path = __DIR__ . '/../config/config.php';
        $app_path = config_path('menuproxy.php');
        $this->mergeConfigWith($package_path, $app_path, 'smorken/menuproxy::config');
        $this->publishes([$package_path => config_path('menuproxy.php')], 'config');
    }

    protected function mergeConfigWith($package_path, $app_path, $key)
    {
        if (file_exists($app_path)) {
            $app_config = require $app_path;
        } else {
            $app_config = [];
        }
        $package_config = require $package_path;
        $this->app['config']->set($key, array_merge($package_config, $app_config));
    }

    public function register()
    {
        $this->app->bind(
            Storage::class,
            function ($app) {
                $mpconfig = $this->app['config']->get('smorken/menuproxy::config', []);
                $scls = array_get($mpconfig, 'storage_class', \Smorken\MenuProxy\Storage\Example::class);
                $menu = $app['config']->get('menu', []);
                return new $scls($menu);
            }
        );
        $this->app->bind(
            Proxy::class,
            function ($app) {
                $mpconfig = $this->app['config']->get('smorken/menuproxy::config', []);
                $service = array_get($mpconfig, 'menu_service_lookup', 'menu');
                $menu = $app[$service];
                $pcls = array_get($mpconfig, 'proxy_class', \Smorken\MenuProxy\Proxies\Lavary::class);
                $popts = array_get($mpconfig, 'proxy_options', []);
                $provider = $app[Storage::class];
                return new $pcls($menu, $popts, $provider);
            }
        );
    }
}
