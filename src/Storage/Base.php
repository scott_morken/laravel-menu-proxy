<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 1/27/16
 * Time: 11:23 AM
 */

namespace Smorken\MenuProxy\Storage;

class Base
{

    protected $menus = [];

    /**
     * @return array
     */
    public function asArray()
    {
        return $this->menus;
    }

    /**
     * Checks the permissions for the requested group
     * It creates a method based on studly casing
     * the group and prepends it with 'check'
     * ie. 'home' => 'checkHome', 'admin' => 'checkAdmin',
     * 'foo_bar' => 'checkFooBar'
     * The method must return a boolean
     * @param string $group
     * @return bool
     */
    protected function verifyPermissions($group)
    {
        $permission = true;
        $methodname = 'check' . ucfirst($group);
        if (method_exists($this, $methodname)) {
            $permission = $this->$methodname();
        }
        return $permission;
    }

    protected function handleModify(&$menus, &$index)
    {
        $menu = $menus[$index];
        if (isset($menu['modify'])) {
            $methodname = $menu['modify'];
            if (method_exists($this, $methodname)) {
                return $this->$methodname($index, $menu);
            } else {
                user_error("$methodname modification does not exist for $index.", E_USER_WARNING);
            }
        }
        return true;
    }

    protected function isTesting()
    {
        return app('APP_ENV') === 'testing';
    }
}
