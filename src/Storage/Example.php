<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 1/27/16
 * Time: 11:22 AM
 */

namespace Smorken\MenuProxy\Storage;

use Smorken\MenuProxy\Contracts\Storage;

class Example extends FromArray implements Storage
{

    protected function checkAdmin()
    {

        $rules = app('rbac.rules');
        if (method_exists($rules, 'getRulesFromRoute')) {
            $route = \Route::current();
            if (!$route) {
                return false;
            }
            return \Auth::check() && $rules->check($route, config('rbac.admin', []));
        }

        return \Auth::check() && $rules->check('*', config('rbac.admin', []));
    }

    protected function checkAuthOnly()
    {
        return \Auth::check();
        /* example of using $rules->check with @ (authenticated) roles
        $rules = app('rbac.rules');
        return \Auth::check() && $rules->check('*', array(
            'allow' => array(
                'actions' => array('*'),
                'roles' => array('@'),
                //'users' => array('user_id1', 'user_id2'),
            ),
            'deny' => array(
                '*'
            ),
        ));
        */
    }

    protected function modifyLogin(&$name, &$item)
    {
        return \Auth::guest();
    }

    protected function modifyLogout(&$name, &$item)
    {
        $auth = \Auth::check() && \Auth::user();
        if ($auth) {
            $username = \Auth::user()->getLogin();
            $name = $name . ' (' . $username . ')';
        }
        return $auth;
    }
}
