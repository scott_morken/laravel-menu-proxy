<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 1/27/16
 * Time: 10:55 AM
 */

namespace Smorken\MenuProxy\Storage;

abstract class FromArray extends Base
{

    protected $model = [];

    /**
     * FromArray constructor.
     * @param array $menus
     */
    public function __construct($menus)
    {
        $this->model = $menus;
        $this->buildMenuArray($menus);
    }

    protected function buildMenuArray($model)
    {
        $actualmenus = [];
        foreach ($model as $group => $menus) {
            if ($this->verifyPermissions($group)) {
                $actualmenus = $this->addMenusToArray($menus, $actualmenus);
            }
        }
        $this->menus = $actualmenus;
    }

    protected function addMenusToArray($menus, $menu_array = [])
    {
        foreach ($menus as $name => $menuopts) {
            if ($this->handleModify($menus, $name)) {
                $menu_array[$name] = $menuopts;
                if (isset($menuopts['children'])) {
                    $menu_array[$name]['children'] = $this->addMenusToArray($menuopts['children']);
                }
            }
        }
        return $menu_array;
    }
}
