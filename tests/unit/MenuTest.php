<?php

use Mockery as m;

class MenuTest extends \PHPUnit\Framework\TestCase
{

    public function tearDown()
    {
        m::close();
        parent::tearDown();
    }

    public function testMenuArrayWithoutAuthHasLogin()
    {
        $auth = m::mock('alias:\Auth');
        $auth->shouldReceive('guest')->andReturn(true);
        $auth->shouldReceive('check')->andReturn(false);
        $menu = $this->getMenuWithAuth();
        $marray = $menu->asArray();
        $this->assertArrayHasKey('Login', $marray);
    }

    protected function getMenuWithAuth()
    {
        $menu['auth'] = [
            'Login'  => [
                'route'  => 'login',
                'modify' => 'modifyLogin',
            ],
            'Logout' => [
                'route'  => 'logout',
                'modify' => 'modifyLogout',
            ],
        ];
        return new \Smorken\MenuProxy\Storage\Example($menu);
    }

    public function testMenuArrayWithAuthHasModdedLogout()
    {
        $usermock = m::mock('stdClass');
        $usermock->shouldReceive('getLogin')->andReturn('foo');
        $auth = m::mock('alias:\Auth');
        $auth->shouldReceive('guest')->andReturn(false);
        $auth->shouldReceive('check')->andReturn(true);
        $auth->shouldReceive('user')->andReturn($usermock);
        $menu = $this->getMenuWithAuth();
        $marray = $menu->asArray();
        $this->assertArrayHasKey('Logout (foo)', $marray);
    }

    public function testMenuArrayWithChildrenContainsChildren()
    {
        $menu = $this->getMenuWithChildren();
        $marray = $menu->asArray();
        $this->assertArrayHasKey('Users', $marray['Admin']['children']);
    }

    protected function getMenuWithChildren()
    {
        $menu['na'] = [
            'Admin' => [
                'action'   => 'Admin\AdminController@index',
                'children' => [
                    'Users' => [
                        'action' => 'Admin\User\UserController@getIndex',
                    ],
                ],
            ],
        ];
        return new \Smorken\MenuProxy\Storage\Example($menu);
    }
}
