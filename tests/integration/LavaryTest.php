<?php
use Smorken\MenuProxy\Proxies\Lavary;

/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 1/27/16
 * Time: 9:54 AM
 */
class LavaryTest extends BaseTestCase
{

    public function testLavaryIsBuiltIsFalse()
    {
        $lp = $this->app['Smorken\MenuProxy\Contracts\Proxy'];
        $built = $lp->isBuilt();
        $this->assertFalse($built);
    }

    public function testLavaryBuildIsBuiltIsTrue()
    {
        $lp = $this->app['Smorken\MenuProxy\Contracts\Proxy'];
        $this->assertTrue($lp->build());
    }

    public function testLavaryBuildWithChildrenHasChildren()
    {
        $lp = $this->getLavaryWithChildren();
        $lp->build();
        $this->assertTrue($lp->getUsable()->admin->hasChildren());
    }

    protected function getLavaryWithChildren()
    {
        return new Lavary(
            new \Lavary\Menu\Menu(),
            config('smorken/menuproxy::config.proxy_options', []),
            $this->getMenuWithChildren()
        );
    }

    protected function getMenuWithChildren()
    {
        return new \Smorken\MenuProxy\Storage\Example($this->getMenuArrayWithChildren());
    }

    protected function getMenuArrayWithChildren()
    {
        $menu['na'] = [
            'Admin' => [
                'url'      => 'foo',
                'children' => [
                    'Users' => [
                        'url' => 'foo/users',
                    ],
                    'Other' => [
                        'url'      => 'admin/other',
                        'children' => [
                            'Foo' => [
                                'url' => 'admin/other/foo',
                            ],
                        ],
                    ],
                ],
            ],
        ];
        return $menu;
    }

    public function testLavaryRenderBase()
    {
        $lp = $this->getLavaryWithChildren();
        $str = $lp->render();
        $this->assertContains('href="http://localhost/foo"', $str);
    }

    public function testLavaryGetSubmenuSingle()
    {
        $this->app['config']->set('menu', $this->getMenuArrayWithChildren());
        $lp = $this->app['Smorken\MenuProxy\Contracts\Proxy'];
        $m = $lp->getSubmenu('admin');
        $this->assertCount(2, $m);
        $this->assertInstanceOf('\Illuminate\Support\Collection', $m);
    }

    public function testLavaryGetSubmenuDotted()
    {
        $this->app['config']->set('menu', $this->getMenuArrayWithChildren());
        $lp = $this->app['Smorken\MenuProxy\Contracts\Proxy'];
        $m = $lp->getSubmenu('admin.other');
        $this->assertCount(1, $m);
        $this->assertInstanceOf('\Illuminate\Support\Collection', $m);
    }
}
