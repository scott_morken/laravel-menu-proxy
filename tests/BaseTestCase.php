<?php

abstract class BaseTestCase extends Orchestra\Testbench\TestCase
{

    protected function getPackageProviders($app)
    {
        return [
            \Smorken\MenuProxy\ServiceProvider::class,
            \Lavary\Menu\ServiceProvider::class,
        ];
    }
}
